package br.com.pupposoft.biometric.client.gateways.database.filesystem.json;

import br.com.pupposoft.biometric.client.domains.User;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class UserJson {
	private String userName;
	private String templates;
	
	public UserJson(final User user) {
		this.userName = user.getUserName();
		this.templates = user.getTemplates();
	}
}
