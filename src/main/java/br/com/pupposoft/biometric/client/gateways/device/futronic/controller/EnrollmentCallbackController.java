package br.com.pupposoft.biometric.client.gateways.device.futronic.controller;

import java.awt.image.BufferedImage;

import org.apache.commons.codec.binary.Base64;

import com.futronic.SDKHelper.FTR_PROGRESS;
import com.futronic.SDKHelper.FutronicEnrollment;
import com.futronic.SDKHelper.FutronicSdkBase;
import com.futronic.SDKHelper.IEnrollmentCallBack;

import br.com.pupposoft.biometric.client.domains.BiometricStatus;
import br.com.pupposoft.biometric.client.domains.User;
import br.com.pupposoft.biometric.client.gateways.database.DatabaseGateway;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EnrollmentCallbackController implements IEnrollmentCallBack {
	private BiometricStatus status;
	private String userName;
	private DatabaseGateway biometricApiGateway;
	private FutronicEnrollment futronicEnrollment;
	
	public EnrollmentCallbackController(
			final String userName,
			final DatabaseGateway biometricApiGateway,	
			final FutronicEnrollment futronicEnrollment
			) {
		this.userName = userName;
		this.biometricApiGateway = biometricApiGateway;
		this.futronicEnrollment = futronicEnrollment;
	}
	
	@Override
	public boolean OnFakeSource(FTR_PROGRESS progress) {
		return false;
	}

	@Override
	public void OnPutOn(FTR_PROGRESS progress) {
		System.out.println(progress.m_Count + "/" + progress.m_Total);
		this.status = BiometricStatus.ON_PUT_ON;
	}

	@Override
	public void OnTakeOff(FTR_PROGRESS progress) {
		this.status = BiometricStatus.ON_TAKE_OFF;
	}

	@Override
	public void UpdateScreenImage(BufferedImage bitmap) {
		//Este metodo recebe como parâmetro a imagem do dedo, caso queira exibir na tela
		this.status = BiometricStatus.UPDATE_SCREEN_IMAGE;
	}

	@Override
	public void OnEnrollmentComplete(final boolean isSuccess, final int result) {
		if(isSuccess) {
			final User user = new User(this.userName, Base64.encodeBase64String(this.futronicEnrollment.getTemplate()));
			this.biometricApiGateway.save(user);
			this.status = BiometricStatus.REGISTRATION_SUCCESSFUL;
		} else {
			System.err.println("Error: Ftr return Code ("+ result +"): " + FutronicSdkBase.SdkRetCode2Message(result));
			this.status = BiometricStatus.UNEXPECTED_ERROR;
		}
	}
}
