package br.com.pupposoft.biometric.client.usecases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.pupposoft.biometric.client.domains.User;
import br.com.pupposoft.biometric.client.gateways.database.DatabaseGateway;
import br.com.pupposoft.biometric.client.usecases.exceptions.UserAlreadyExistsBussinesException;

@Service
public class CreateUser {
	
	@Autowired
	private DatabaseGateway dataBaseGateway;
	
	public void create(final User userToCreate) {
		this.checkIfuserAlreadExists(userToCreate);
		
		this.dataBaseGateway.save(userToCreate);
	}

	private void checkIfuserAlreadExists(final User userToCreate) {
		final boolean userLreadyExist = 
				this.dataBaseGateway.getUsers().stream()
					.anyMatch(userExistent -> userExistent.getUserName().equals(userToCreate.getUserName()));
		
		if(userLreadyExist) {
			throw new UserAlreadyExistsBussinesException();
		}
	}
}
