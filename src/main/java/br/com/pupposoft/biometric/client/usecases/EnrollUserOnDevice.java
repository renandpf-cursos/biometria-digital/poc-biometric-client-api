package br.com.pupposoft.biometric.client.usecases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.pupposoft.biometric.client.domains.BiometricStatus;
import br.com.pupposoft.biometric.client.gateways.device.BiometricDeviceGateway;

@Service
public class EnrollUserOnDevice {
	
	@Autowired
	private BiometricDeviceGateway biometricDeviceGateway;

	public void enroll(final String userName) {
		this.biometricDeviceGateway.enroll(userName);
	}

	public BiometricStatus getStatus() {
		return biometricDeviceGateway.getEnrollStatus();
	}
}
