package br.com.pupposoft.biometric.client.gateways.http.controllers.json;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class UserResponseJson {
	private String userName;
}
