package br.com.pupposoft.biometric.client.gateways.database;

import java.util.List;

import br.com.pupposoft.biometric.client.domains.User;

public interface DatabaseGateway {
	public void save(final User user);
	public List<User> getUsers();
}
