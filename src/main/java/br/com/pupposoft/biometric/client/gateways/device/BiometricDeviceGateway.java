package br.com.pupposoft.biometric.client.gateways.device;

import java.util.List;

import br.com.pupposoft.biometric.client.domains.BiometricStatus;
import br.com.pupposoft.biometric.client.domains.User;

public interface BiometricDeviceGateway {
	public void enroll(final String userName);
	public BiometricStatus getEnrollStatus();
	public void identify(String userName, List<User> users);
	public BiometricStatus getIdentifyStatus();
}
