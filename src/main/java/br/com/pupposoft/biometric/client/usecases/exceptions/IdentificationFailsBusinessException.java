package br.com.pupposoft.biometric.client.usecases.exceptions;

import org.springframework.http.HttpStatus;

import br.com.pupposoft.biometric.client.exceptions.BiometricClientBaseException;
import lombok.Getter;

@Getter
public class IdentificationFailsBusinessException extends BiometricClientBaseException {
	private static final long serialVersionUID = -7945746971133750929L;

	private final String code = "biometric.client.api.identificationFails";
	private final String message = "User Identification Fails";
	private final HttpStatus httpStatus = HttpStatus.UNPROCESSABLE_ENTITY;
	private Integer errorCode;

	public IdentificationFailsBusinessException(final int errorCode) {
		this.errorCode = errorCode;
	}
}
