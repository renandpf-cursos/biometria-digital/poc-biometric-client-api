package br.com.pupposoft.biometric.client.gateways.http.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.pupposoft.biometric.client.domains.User;
import br.com.pupposoft.biometric.client.gateways.database.DatabaseGateway;
import br.com.pupposoft.biometric.client.gateways.http.controllers.json.UserJson;
import br.com.pupposoft.biometric.client.usecases.CreateUser;

@RestController
@RequestMapping("user")
@CrossOrigin("*")
public class UserController {
	
	@Autowired
	private CreateUser createUser;
	
	@Autowired
	private DatabaseGateway databaseGateway;
	
	@PostMapping()
	public void create(@RequestBody final UserJson userToCreateJson) {
		final User userToCreate = new User(userToCreateJson.getUserName(), null);
		this.createUser.create(userToCreate);
	}
	
	@GetMapping()
	public List<UserJson> getAll() {
		final List<UserJson> userJson = this.databaseGateway.getUsers().stream().map(UserJson::new).collect(Collectors.toList());
		
		return userJson;
	}
}
