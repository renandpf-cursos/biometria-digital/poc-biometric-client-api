package br.com.pupposoft.biometric.client.gateways.exceptions;

import org.springframework.http.HttpStatus;

import br.com.pupposoft.biometric.client.exceptions.BiometricClientBaseException;
import lombok.Getter;

@Getter
public class ErrorToSaveUserGatewayException extends BiometricClientBaseException {
	private static final long serialVersionUID = -6978957386312435484L;

	private final String code = "biometric.client.api.errorToSaveUser";
	private final String message = "Error to save user. Please, Try again.";
	private final HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
}
