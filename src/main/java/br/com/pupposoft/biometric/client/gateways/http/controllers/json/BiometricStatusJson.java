package br.com.pupposoft.biometric.client.gateways.http.controllers.json;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class BiometricStatusJson {
	private String message;
}
