package br.com.pupposoft.biometric.client.usecases;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import br.com.pupposoft.biometric.client.domains.BiometricStatus;
import br.com.pupposoft.biometric.client.domains.User;
import br.com.pupposoft.biometric.client.gateways.database.DatabaseGateway;
import br.com.pupposoft.biometric.client.gateways.device.BiometricDeviceGateway;
import br.com.pupposoft.biometric.client.usecases.exceptions.NoUsersToAuthorizeBusinessException;

@Service
public class CheckUserAuthorizationOnDevice {
	
	@Autowired
	private BiometricDeviceGateway biometricDeviceGateway;
	
	@Autowired
	private DatabaseGateway biometricApiGateway;
	
	public void verify(final String userName) {
		final List<User> usersToSearch = this.biometricApiGateway.getUsers();
		
		if(CollectionUtils.isEmpty(usersToSearch)) {
			throw new NoUsersToAuthorizeBusinessException();
		}
		
		this.biometricDeviceGateway.identify(userName, usersToSearch);
	}

	public BiometricStatus getStatus() {
		return biometricDeviceGateway.getIdentifyStatus();
	}
}
