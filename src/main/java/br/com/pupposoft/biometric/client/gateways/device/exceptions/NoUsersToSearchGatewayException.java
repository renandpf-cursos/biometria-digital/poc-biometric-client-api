package br.com.pupposoft.biometric.client.gateways.device.exceptions;

import org.springframework.http.HttpStatus;

import br.com.pupposoft.biometric.client.exceptions.BiometricClientBaseException;
import lombok.Getter;

@Getter
public class NoUsersToSearchGatewayException extends BiometricClientBaseException {
	private static final long serialVersionUID = -3461292408826533818L;
	public final String code = "poc.biometric.md.client.NoUsersToSearch";
	public final String message = "User list to search is empty";
	public final HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
}
