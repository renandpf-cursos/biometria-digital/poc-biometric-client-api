package br.com.pupposoft.biometric.client.gateways.http.controllers.json;

import br.com.pupposoft.biometric.client.domains.BiometricStatus;
import lombok.Getter;

@Getter
public class BiometricResponseJson {
	private String message;
	private String errorCode;
	private String errorMessage;
	private BiometricStatus biometricStatus;

	public BiometricResponseJson(final String message) {
		super();
		this.message = message;
	}

	public BiometricResponseJson(final String message, final BiometricStatus biometricStatus) {
		super();
		this.message = message;
		this.biometricStatus = biometricStatus;
	}
	
	public BiometricResponseJson(final String errorCode, final String errorMessage) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}
}
