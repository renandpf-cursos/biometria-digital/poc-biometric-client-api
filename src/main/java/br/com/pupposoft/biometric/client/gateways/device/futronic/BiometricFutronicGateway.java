package br.com.pupposoft.biometric.client.gateways.device.futronic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.futronic.SDKHelper.FutronicEnrollment;
import com.futronic.SDKHelper.FutronicIdentification;

import br.com.pupposoft.biometric.client.domains.BiometricStatus;
import br.com.pupposoft.biometric.client.domains.User;
import br.com.pupposoft.biometric.client.gateways.database.DatabaseGateway;
import br.com.pupposoft.biometric.client.gateways.device.BiometricDeviceGateway;
import br.com.pupposoft.biometric.client.gateways.device.exceptions.NoUsersToSearchGatewayException;
import br.com.pupposoft.biometric.client.gateways.device.futronic.controller.EnrollmentCallbackController;
import br.com.pupposoft.biometric.client.gateways.device.futronic.controller.IdentificationCallBackController;


@Component
public class BiometricFutronicGateway implements BiometricDeviceGateway {

	@Autowired
	private FutronicEnrollment futronicEnrollment; 
	private EnrollmentCallbackController enrollmentCallbackController;

	@Autowired
	private FutronicIdentification futronicIdentification; 
	private IdentificationCallBackController identificationCallBackController;

	@Autowired
	private DatabaseGateway biometricApiGateway; 
	
	@Override
	public void enroll(final String userName) {
		this.enrollmentCallbackController = new EnrollmentCallbackController(
				userName,
				this.biometricApiGateway,
				this.futronicEnrollment);
		
		futronicEnrollment.Enrollment(this.enrollmentCallbackController);
	}

	@Override
	public BiometricStatus getEnrollStatus() {
		if(this.enrollmentCallbackController == null) {
			return BiometricStatus.PROCESS_NOT_STARTED;
		}
		return this.enrollmentCallbackController.getStatus();
	}

	@Override
	public void identify(final String userName, final List<User> usersToSearch) {
		if(CollectionUtils.isEmpty(usersToSearch)) {
			throw new NoUsersToSearchGatewayException();
		}

		this.identificationCallBackController = new IdentificationCallBackController(
				this.futronicIdentification,
				usersToSearch);
		this.futronicIdentification.GetBaseTemplate(this.identificationCallBackController);
	}

	@Override
	public BiometricStatus getIdentifyStatus() {
		if(this.identificationCallBackController == null) {
			return BiometricStatus.PROCESS_NOT_STARTED;
		}
		return this.identificationCallBackController.getStatus();
	}
}
