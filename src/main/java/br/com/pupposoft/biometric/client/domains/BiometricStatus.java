package br.com.pupposoft.biometric.client.domains;

import lombok.Getter;

@Getter
public enum BiometricStatus {
	PROCESS_NOT_STARTED,
	WAITING,
	ON_PUT_ON,
	ON_TAKE_OFF,
	UPDATE_SCREEN_IMAGE,
	ON_GET_BASE_TEMPLATE_START,
	PROCCESS_IDENTIFY_COMPLETED,
	REGISTRATION_SUCCESSFUL,
	UNEXPECTED_ERROR,
	USER_NOT_FOUND;
}
