package br.com.pupposoft.biometric.client.usecases.exceptions;

import org.springframework.http.HttpStatus;

import br.com.pupposoft.biometric.client.exceptions.BiometricClientBaseException;
import lombok.Getter;

@Getter
public class NoUsersToAuthorizeBusinessException extends BiometricClientBaseException {
	private static final long serialVersionUID = -3461292408826533818L;

	private final String code = "biometric.client.api.NoUsersToAuthorize";
	private final String message = "Ops! A lista de usuários na base de dados está vazia! Cadastre pelo menos um usuário.";
	private final HttpStatus httpStatus = HttpStatus.UNPROCESSABLE_ENTITY;
}
