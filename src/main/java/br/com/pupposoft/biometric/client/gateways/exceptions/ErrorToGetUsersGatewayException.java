package br.com.pupposoft.biometric.client.gateways.exceptions;

import org.springframework.http.HttpStatus;

import br.com.pupposoft.biometric.client.exceptions.BiometricClientBaseException;
import lombok.Getter;

@Getter
public class ErrorToGetUsersGatewayException extends BiometricClientBaseException {
	private static final long serialVersionUID = 665670251531936130L;

	private final String code = "biometric.client.api.errorToGetUsers";
	private final String message = "Error to get users. Try again";
	private final HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
}
