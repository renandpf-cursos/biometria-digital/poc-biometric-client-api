package br.com.pupposoft.biometric.client.gateways.database.filesystem;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.pupposoft.biometric.client.domains.User;
import br.com.pupposoft.biometric.client.gateways.database.DatabaseGateway;
import br.com.pupposoft.biometric.client.gateways.exceptions.ErrorToGetUsersGatewayException;
import br.com.pupposoft.biometric.client.gateways.exceptions.ErrorToSaveUserGatewayException;

@Component
public class UnixFileSystem implements DatabaseGateway {
	
	@Value("${database.filesystem.path}")
	private String basePath;

	@Override
	public void save(final User user) {
		try {
			this.createDir();
			final ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.writeValue(new File(this.basePath + "/" + user.getUserName()+".json"), user);
		} catch (final Exception e) {
			e.printStackTrace();
			throw new ErrorToSaveUserGatewayException();
		}
	}

	@Override
	public List<User> getUsers() {
		final ObjectMapper objectMapper = new ObjectMapper();
		final List<User> users = new ArrayList<>();
		final List<String> filesNames = new ArrayList<>();
		try {
			this.createDir();
			Files.list(Paths.get(this.basePath)).forEach(path -> {
				filesNames.add(path.toString());
			});

			for (String fileName : filesNames) {
				User user = objectMapper.readValue(new File(fileName), User.class);
				users.add(user);
			}
			
			return users;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ErrorToGetUsersGatewayException();
		}
	}
	
	private void createDir() throws IOException {
		Files.createDirectories(Paths.get(this.basePath));
	}
	
}
