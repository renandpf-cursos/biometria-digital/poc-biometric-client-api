package br.com.pupposoft.biometric.client.usecases.exceptions;

import org.springframework.http.HttpStatus;

import br.com.pupposoft.biometric.client.exceptions.BiometricClientBaseException;
import lombok.Getter;

@Getter
public class UserNotFoundBusinessException extends BiometricClientBaseException {
	private static final long serialVersionUID = -6845188932439203143L;
	
	private final String code = "biometric.client.api.userNotFound";
	private final String message = "Ops! Usuário não encontrado.";
	private final HttpStatus httpStatus = HttpStatus.NOT_FOUND;
}
