package br.com.pupposoft.biometric.client.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.futronic.SDKHelper.FutronicEnrollment;
import com.futronic.SDKHelper.FutronicException;
import com.futronic.SDKHelper.FutronicIdentification;
import com.futronic.SDKHelper.VersionCompatible;

@Configuration
public class FutronicBeansConfig {
	@Bean
	public FutronicEnrollment getFutronicEnrollment() throws FutronicException {
		
		final FutronicEnrollment operation = new FutronicEnrollment();
		
        operation.setFakeDetection( false );
        operation.setFFDControl( true );
        operation.setFARN( 166 );
        operation.setFastMode( false );
        operation.setMIOTControlOff( false );
        operation.setMaxModels( 5 );//Quantidade de vezes q deve ser colocado o dedo
        operation.setVersion(VersionCompatible.ftr_version_previous );
		return operation;
	}

	@Bean
	public FutronicIdentification getFutronicIdentification() throws FutronicException {
		final FutronicIdentification operation = new FutronicIdentification();
		
		operation.setFakeDetection( false );
		operation.setFFDControl( true );
		operation.setFARN( 166 );
		operation.setFastMode( false );
		operation.setVersion(VersionCompatible.ftr_version_previous );
		return operation;
	}
}
