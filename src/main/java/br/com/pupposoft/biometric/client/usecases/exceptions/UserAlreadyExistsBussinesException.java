package br.com.pupposoft.biometric.client.usecases.exceptions;

import org.springframework.http.HttpStatus;

import br.com.pupposoft.biometric.client.exceptions.BiometricClientBaseException;
import lombok.Getter;

@Getter
public class UserAlreadyExistsBussinesException extends BiometricClientBaseException {
	private static final long serialVersionUID = 2820068390640788189L;
	
	private final String code = "biometric.client.api.userAlreadExistis";
	private final String message = "Ops! Já existe um usuário cadastro com esse user name!";
	private final HttpStatus httpStatus = HttpStatus.UNPROCESSABLE_ENTITY;
}
