package br.com.pupposoft.biometric.client.gateways.http.controllers.json;

import lombok.Setter;
import br.com.pupposoft.biometric.client.domains.User;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Setter
@NoArgsConstructor
public class UserJson {
	private String userName;
	
	public UserJson(final User user) {
		this.userName = user.getUserName();
	}
}
