package br.com.pupposoft.biometric.client.gateways.http.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.pupposoft.biometric.client.domains.BiometricStatus;
import br.com.pupposoft.biometric.client.exceptions.BiometricClientBaseException;
import br.com.pupposoft.biometric.client.gateways.http.controllers.json.BiometricResponseJson;
import br.com.pupposoft.biometric.client.gateways.http.controllers.json.UserJson;
import br.com.pupposoft.biometric.client.usecases.CheckUserAuthorizationOnDevice;
import br.com.pupposoft.biometric.client.usecases.EnrollUserOnDevice;

@RestController
@RequestMapping("biometric")
@CrossOrigin("*")
public class BiometricController {

	@Autowired
	private EnrollUserOnDevice enrollUser; 
	
	@Autowired
	private CheckUserAuthorizationOnDevice checkUserAuthorizationOnDevice;
	
	@PostMapping("request-enroll-user")
	public ResponseEntity<BiometricResponseJson> enrollUser(@RequestBody final UserJson userToEnroll) {
		try {
			enrollUser.enroll(userToEnroll.getUserName());
			
			return new ResponseEntity<BiometricResponseJson>(new BiometricResponseJson("Solicita��o de Cadastramento realizada com sucesso!"), HttpStatus.OK);
			
		} catch (BiometricClientBaseException e) {
			e.printStackTrace();
			return new ResponseEntity<BiometricResponseJson>(new BiometricResponseJson(e.getCode(), e.getMessage()), e.getHttpStatus());
		} catch (Exception e) {
			return this.throwsUnexpectedError(e);
		}
	}

	@GetMapping("request-enroll-user-status")
	public ResponseEntity<BiometricResponseJson> enrollUserStatus() {
		return new ResponseEntity<BiometricResponseJson>(
				new BiometricResponseJson(this.mapperBiometricStatusToDescription(enrollUser.getStatus()), enrollUser.getStatus()), HttpStatus.OK);
	}
	
	@PostMapping("request-authorize")
	public ResponseEntity<BiometricResponseJson> authorizeUser(@RequestBody final UserJson userToAuthorize) {
		try {
			this.checkUserAuthorizationOnDevice.verify(userToAuthorize.getUserName());
			
			return new ResponseEntity<BiometricResponseJson>(new BiometricResponseJson("Solicita��o de Autoriza��o realizada com sucesso!"), HttpStatus.OK);
		} catch (BiometricClientBaseException e) {
			e.printStackTrace();
			return new ResponseEntity<BiometricResponseJson>(new BiometricResponseJson(e.getCode(), e.getMessage()), e.getHttpStatus());
		} catch (Exception e) {
			return this.throwsUnexpectedError(e);
		}
	}

	@GetMapping("request-authorize-status")
	public ResponseEntity<BiometricResponseJson> authorizeUserStatus() {
		return new ResponseEntity<BiometricResponseJson>(
				new BiometricResponseJson(this.mapperBiometricStatusToDescription(this.checkUserAuthorizationOnDevice.getStatus()), enrollUser.getStatus()), HttpStatus.OK);
	}
	
	private ResponseEntity<BiometricResponseJson> throwsUnexpectedError(Exception e) {
		e.printStackTrace();
		return new ResponseEntity<BiometricResponseJson>(new BiometricResponseJson(
				"poc.biometric.md.client.unexpectedError", 
				"Ops! Ocorreu um erro inesperado! Tente novamente."), 
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	
	private String mapperBiometricStatusToDescription(BiometricStatus status) {

		if(status == null) {
			status = BiometricStatus.PROCESS_NOT_STARTED;
		}
		
		switch (status) {
		case ON_GET_BASE_TEMPLATE_START:
			return ""; 
			
		case ON_PUT_ON:
			return "Pressione o dedo no sensor...";
			
		case ON_TAKE_OFF:
			return "Retire o dedo do sensor...";
			
		case PROCCESS_IDENTIFY_COMPLETED:
			return "Identifica��o realizado com sucesso!";
			
		case PROCESS_NOT_STARTED:
			return "Processo de leitura n�o iniciado.";
			
		case REGISTRATION_SUCCESSFUL:
			return "Registro ocorreu com sucesso!";
			
		case UNEXPECTED_ERROR:
			 return "Ops! Desculpe ocorreu um erro. Tente novamente.";
			
		case UPDATE_SCREEN_IMAGE:
			return "Obtendo imagem da leitura.";
			
		case USER_NOT_FOUND:
			return "Ops! Usu�rio n�o encontrado!";
			
		case WAITING:
			return "Aguardando leitura...";
		}
		
		System.err.println("Status n�o mapeado: "+ status);
		return "Ops! Desculpe ocorreu um erro. Tente novamente.";
	}
	
}
