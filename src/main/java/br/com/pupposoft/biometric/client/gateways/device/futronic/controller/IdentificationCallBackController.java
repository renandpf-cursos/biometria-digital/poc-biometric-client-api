package br.com.pupposoft.biometric.client.gateways.device.futronic.controller;

import java.awt.image.BufferedImage;
import java.util.List;

import org.apache.commons.codec.binary.Base64;

import com.futronic.SDKHelper.FTR_PROGRESS;
import com.futronic.SDKHelper.FtrIdentifyRecord;
import com.futronic.SDKHelper.FtrIdentifyResult;
import com.futronic.SDKHelper.FutronicIdentification;
import com.futronic.SDKHelper.FutronicSdkBase;
import com.futronic.SDKHelper.IIdentificationCallBack;

import br.com.pupposoft.biometric.client.domains.BiometricStatus;
import br.com.pupposoft.biometric.client.domains.User;
import br.com.pupposoft.biometric.client.usecases.exceptions.IdentificationFailsBusinessException;
import br.com.pupposoft.biometric.client.usecases.exceptions.UserNotFoundBusinessException;
import lombok.Getter;

@Getter
public class IdentificationCallBackController implements IIdentificationCallBack {
	private BiometricStatus status;
	private FutronicIdentification futronicIdentification;
	private List<User> allUsers;

	public IdentificationCallBackController(
			final FutronicIdentification futronicIdentification,
			final List<User> allUsers) {
		this.futronicIdentification = futronicIdentification;
		this.allUsers = allUsers;
		this.status = BiometricStatus.WAITING;
	}
	
	@Override
	public boolean OnFakeSource(FTR_PROGRESS progress) {
		return false;
	}

	@Override
	public void OnPutOn(FTR_PROGRESS progress) {
		this.status = BiometricStatus.ON_PUT_ON;
	}

	@Override
	public void OnTakeOff(FTR_PROGRESS progress) {
		this.status = BiometricStatus.ON_TAKE_OFF;
	}

	@Override
	public void UpdateScreenImage(final BufferedImage bitmap) {
		//Este metodo recebe como par�metro a imagem do dedo, caso queira exibir na tela
		this.status = BiometricStatus.UPDATE_SCREEN_IMAGE;
	}
	
	@Override
	public void OnGetBaseTemplateComplete(boolean isCaptorSuccess, int result) {
		this.status = BiometricStatus.ON_GET_BASE_TEMPLATE_START;
		if(isCaptorSuccess) {
			FtrIdentifyRecord ftrIndetifyRecords[] = mapperToFtrIdentifyRecords();
	
			try {
				final FtrIdentifyResult ftrResult = new FtrIdentifyResult();
				final int ftrIdentificationResultCode = this.futronicIdentification.Identification(ftrIndetifyRecords, ftrResult);
				
				try {
					
					//TODO: executar alguma ação, pois a digital foi reconhecida!
					
					System.out.println("User Found!");
					this.status = BiometricStatus.PROCCESS_IDENTIFY_COMPLETED;
					
				} catch (final UserNotFoundBusinessException e) {
					e.printStackTrace();
					this.status = BiometricStatus.USER_NOT_FOUND;
					
				} catch (final IdentificationFailsBusinessException e) {
					e.printStackTrace();
					System.out.println("Ftr Identification Return Code ("+ ftrIdentificationResultCode +"): " + FutronicSdkBase.SdkRetCode2Message(ftrIdentificationResultCode));
					this.status = BiometricStatus.UNEXPECTED_ERROR;
				} 
				
			} catch (Exception e) {
				e.printStackTrace();
				this.status = BiometricStatus.UNEXPECTED_ERROR;
			}
			
			
		} else {
			System.err.println("Error: Ftr return Code ("+ result +"): " + FutronicSdkBase.SdkRetCode2Message(result));
			this.status = BiometricStatus.UNEXPECTED_ERROR;
		}
	}


	private FtrIdentifyRecord[] mapperToFtrIdentifyRecords() {
		final FtrIdentifyRecord ftrIdentifyRecords[] = new FtrIdentifyRecord[this.allUsers.size()];
		for (int i = 0; i < ftrIdentifyRecords.length; i++) {
			final User user = this.allUsers.get(i);
			
			ftrIdentifyRecords[i] = new FtrIdentifyRecord();
			ftrIdentifyRecords[i].m_KeyValue = user.getUserName().getBytes();
			ftrIdentifyRecords[i].m_Template = Base64.decodeBase64(user.getTemplates());
		}
		
		return ftrIdentifyRecords;
	}
}