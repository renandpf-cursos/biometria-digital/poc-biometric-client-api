package br.com.pupposoft.biometric.client.domains;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "templates")
public class User {
	private String userName; //Deve ser unico por usuario!
	private String templates;
}
